import os, easygui,zipfile,gzip
from tqdm import *
#-----------------------------------------------------------------------------
def main():
    workfolder = easygui.diropenbox()
    for root,dir,fileName in os.walk(workfolder):
        for file in tqdm(fileName):
            if ".zip" in file:
                unzip(root,file)
            elif ".gz" in file:
                ungzip(root,file)
#-----------------------------------------------------------------------------
def ungzip(root, fileName):
    #print("Processing: %s" % (fileName))
    inF = gzip.open(os.path.join(root, fileName), 'rb')
    outF = open(os.path.join(root, fileName) + '.txt', 'wb')
    outF.write(inF.read())
    inF.close()
    outF.close()
    #walkFiles(oDir)
#-----------------------------------------------------------------------------
def unzip(root, fileName):
    #print("Processing: %s" % (fileName))
    zipfile.ZipFile(os.path.join(root, fileName)).extractall(os.path.join(root, os.path.splitext(fileName)[0]))
    walkFiles(root, os.path.join(root, os.path.splitext(fileName)[0]))
#-----------------------------------------------------------------------------
def walkFiles(root, fileName):
    path = os.path.join(root, fileName)
    #print ("walking the files of %s" % path)
    for (dirPath, dirNames, fileNames) in os.walk(path):
        for fileName in fileNames:
            if '.zip' in fileName:
                unzip(dirPath, fileName)
            elif '.gz' in fileName:
                ungzip(dirPath,fileName)
#-----------------------------------------------------------------------------
main()